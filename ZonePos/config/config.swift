//
//  config.swift
//  ZonePos
//
//  Created by Victor on 02/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

class CONFIG{
    
    static let APPKEY = "2MeCWdm2HYaDV7lLTvpRoFqCDWxIZ82R"
    static let BASE_URL = "http://dev.trublend.cloud:8000/api/workflow/goto"
    static let APPID = "26715da8-1f2a-40d0-b1c0-f70e5a36c167"
    static let APP_VERSION = "2.0"
    static let USER_LOGIN_FLOW = "ffef2201-304a-4634-9f75-c94bb2d482cf"
    static let INSTITUTION_CODE = "92"
    static let ZONE_CHANNEL_INFO = "embedded"
}
