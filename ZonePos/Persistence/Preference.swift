//
//  Preference.swift
//  ZonePos
//
//  Created by Victor on 08/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation


class AppPref{
    static let userDefault =  UserDefaults.standard
    static let TOKEN_KEY = "token"
    
    static func setToken(token: String) {
        userDefault.set(token, forKey: TOKEN_KEY)
    }
    
    static func getToken() -> String{
        return userDefault.string(forKey: TOKEN_KEY) ?? ""
    }
    
}
