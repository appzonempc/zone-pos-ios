//
//  AuthInitResponse.swift
//  ZonePos
//
//  Created by Victor on 02/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import SwiftyJSON

class AuthInitResponse :Response{
    var token: String = ""
    
    override init(){}

    func fromMapClient(json: JSON) {
        
        var stringResponse: String = json["EventData"]["Response"].stringValue
        
        do {
           var responseDictionary = try convertToDictionary(from: stringResponse)
        
           super.fromMap(json: responseDictionary)
           if let newtoken = responseDictionary["Token"]{
                self.token = newtoken
            }
            
        } catch {
            print(error)
        }
    }
    
    func convertToDictionary(from text: String) throws -> [String: String] {
        guard let data = text.data(using: .utf8) else { return [:] }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: String] ?? [:]
    }
}
