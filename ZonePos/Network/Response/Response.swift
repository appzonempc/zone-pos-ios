//
//  Response.swift
//  ZonePos
//
//  Created by Victor on 02/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import SwiftyJSON

class Response {
    static let SUCCESS_CODE: String = "0";
    static let REQUEST_OTP_CODE: String = "1";
    static let ERROR_CODE: String = "10";
    
    var code: String = ""
    var statusDescription: String = ""
//
//    init(code: String, statusDescription: String){
//        self.code = code
//        self.statusDescription = statusDescription
//    }
    
    func fromMap(json: Dictionary<String, String>){
        
        if let newCode = json["StatusCode"]{
             self.code = newCode
        }
        
        if let statDescription = json["StatusDescription"]{
            self.statusDescription = statDescription
        }
        
    }
    
    
}
