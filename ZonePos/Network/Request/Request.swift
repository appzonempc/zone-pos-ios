//
//  Request.swift
//  ZonePos
//
//  Created by Victor on 02/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

public class Request{
    var  flowId :String = ""
    var institutionCode: String = ""
    var deviceId: String = ""
    var deviceModel: String = ""
    var appId: String = ""
    var appVersion: String = ""
    var channelInfo: String = ""
    
    
    init(deviceId:String, flowId:String, institutionCode:String, deviceModel:String, appid:String, appVersion:String, channelInfo:String){
     
        self.flowId = flowId
        self.institutionCode = institutionCode
        self.deviceId = deviceId
        self.deviceModel = deviceModel
        self.appId = appid
        self.appVersion = appVersion
        self.channelInfo = channelInfo
    }
    
    func toMap() -> Dictionary<String, Any>{
        var standardData = [String:String]()
        
        standardData["DeviceId"] = self.deviceId
        standardData["DeviceModel"] = self.deviceModel
        standardData["AppId"] = self.appId
        standardData["AppVersion"] = self.appVersion
        standardData["Channelinfo"] = self.channelInfo
        standardData["Channelinfo"] = self.channelInfo
        
        var standardRequest = [String:Any]()
        standardRequest["StandardRequest"] = standardData
        
        var instructionData = [String:Any]()
        instructionData["FlowId"] = self.flowId
        instructionData["InstitutionCode"] = self.institutionCode
        
        var requestData = [String: Any]()
        requestData["instruction"] = instructionData
        requestData["data"] = standardRequest
       
        return requestData
    }
    
}
