//
//  AuthInitRequest.swift
//  ZonePos
//
//  Created by Victor on 02/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import SwiftyJSON

class AuthInitRequest : Request{
 
    var phoneNumber:String = ""
    var email: String = ""
    var firstname: String = ""
    var lastname: String = ""
    var key : String = ""
    var serializedAccounts: String = ""
    
    
    
    init(_deviceId: String,
         _deviceModel: String,
         _appId: String,
         _appVersion: String,
         _flowId: String,
         _institutionCode: String,
         _channelinfo: String){
        super.init(deviceId: _deviceId, flowId: _flowId, institutionCode: _institutionCode, deviceModel: _deviceModel, appid: _appId, appVersion: _appVersion, channelInfo: _channelinfo)
    }
    
    
    func toJson() -> Dictionary<String, Any>{
        var data: Dictionary<String, Any> = super.toMap()
        var data2 = Dictionary<String, Any>()
        
        data2["PhoneNumber"] = self.phoneNumber
        data2["Email"] = self.email
        data2["FirstName"] = self.firstname
        data2["LastName"] = self.lastname
        data2["Key"] = self.key
        data2["Accounts"] = self.serializedAccounts
        var standardRequest:Dictionary<String, Any> = data["data"] as! Dictionary<String, Any>
        data2["StandardRequest"] = standardRequest["StandardRequest"]
        
        data["data"] = data2
        return data
    }
}
