//
//  SearchTextField.swift
//  ZonePos
//
//  Created by Victor on 09/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class SearchTextField: UITextField {

    let padding = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 0)
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        
      return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
         return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.inset(by: padding)
    }
}


