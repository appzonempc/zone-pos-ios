//
//  AuthViewController.swift
//  ZonePos
//
//  Created by Victor on 02/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AuthViewController: UIViewController {

    @IBOutlet var AuthImage: UIImageView!
    @IBOutlet var StatusLabel: UILabel!
    @IBOutlet var Button: UIButton!
    @IBOutlet var progressbar: UIActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Button.isHidden = true
        // Do any additional setup after loading the view.
//        performInitialization()
        moveToZoneHomeScreen()
    }
    

    func performInitialization(){
        
        let encoder = JSONEncoder()
        
        var accountList = [[String:String]]()
        accountList.append(["AccountName":"account1", "AccountNumber":"12345678", "BankCode":"033", "IsDefault":"true"])
        accountList.append(["AccountName":"account2", "AccountNumber":"234568992","BankCode":"033", "IsDefault":"false"])
        var encodedData:Data? = nil
        
        do{
            encodedData = try encoder.encode(accountList)
        }catch{
            
        }
        let request = AuthInitRequest(_deviceId: "345262736772", _deviceModel: "IPhone8", _appId: CONFIG.APPID, _appVersion: CONFIG.APP_VERSION, _flowId: CONFIG.USER_LOGIN_FLOW, _institutionCode: CONFIG.INSTITUTION_CODE, _channelinfo: CONFIG.ZONE_CHANNEL_INFO)
        
        request.phoneNumber = "+2348093685782"
        request.email = "zone@gmail.com"
        request.firstname = "Iostest firstname"
        request.key = CONFIG.APPKEY
        request.lastname = "Iostest lastname"
        request.serializedAccounts = String(data: encodedData!, encoding: .utf8)!
        
    
        print("request is >>>>>>> \(request.toJson())")
        
        let authResponse = AuthInitResponse()
        
        Alamofire.request(CONFIG.BASE_URL, method: .post, parameters:request.toJson(),encoding: JSONEncoding.default).responseJSON{
            response in
            
            if(response.result.isSuccess){
            
                let responseJson: JSON = JSON(response.result.value)
                
                authResponse.fromMapClient(json: responseJson)
                
                print("Response is >>>> \(responseJson)")
                
                if authResponse.code == Response.SUCCESS_CODE {
                    
                    print("token is >>> \(authResponse.token)")
                    AppPref.setToken(token: authResponse.token)
                    
                    self.moveToZoneHomeScreen()
                    
                } else {
                    print("error")
                    print(authResponse.statusDescription)
                    self.resetViews()
                }
            } else{
                self.resetViews()
            }
        }
        
    }
   
    
    func resetViews(){
        self.StatusLabel.text = "Initialization failed"
        self.Button.isHidden = false
        self.progressbar.isHidden = true
    }
    
    func moveToZoneHomeScreen(){
        
        performSegue(withIdentifier: "launchHome", sender: self)
    }
    

    @IBAction func RetryButton(_ sender: Any) {
        self.StatusLabel.text = "Initializating Zone, Please wait"
        self.Button.isHidden = true
        progressbar.isHidden = false
        
        performInitialization()
        
    }
}
