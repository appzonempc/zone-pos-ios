//
//  HomeViewController.swift
//  ZonePos
//
//  Created by Victor on 02/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet var segmentControl: UISegmentedControl!
    
  
    @IBOutlet var historyContainer: UIView!
    @IBOutlet var paymentContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeFirstSegment()
        initializeSegmentedControl()
    }
    
    func initializeSegmentedControl(){
        let font = UIFont.systemFont(ofSize: 18)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
    }

    @IBAction func segmentSelectedAction(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            UIView.animate(withDuration: 0.5) {
               self.initializeFirstSegment()
            }
        case 1:
            UIView.animate(withDuration: 0.5) {
                self.initializeSecondFragment()
            }
            
//            showQrSheet()
            
        default: break
        }
    }
    
    func initializeFirstSegment(){
        historyContainer.isHidden = false
        paymentContainer.isHidden = true
        
    }
    
    func initializeSecondFragment() {
        self.historyContainer.isHidden = true
        self.paymentContainer.isHidden = false
    }
    
    
    
}



