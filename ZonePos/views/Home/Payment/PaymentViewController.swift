//
//  PaymentViewController.swift
//  ZonePos
//
//  Created by Victor on 08/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit
import PullableSheet

class PaymentViewController: UIViewController {

    var amountSheet: PullableSheet?
//    let manager = BottomSheetPresentationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        launchAmountSheet()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
        
         launchAmountSheet()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func buttonPressed(_ sender: Any) {
          launchAmountSheet()
    }
    
    
    
    func launchAmountSheet() {
        
        let amountSheetVC = AmountEntryViewController(nibName: "AmountEntry", bundle: nil)
//        amountSheetVC.view.backgroundColor = .clear
        amountSheetVC.parentSheetDelegate = self
//        amountSheetVC.modalPresentationStyle = .popover
//        amountSheetVC.view.layer.masksToBounds = false
//        amountSheetVC.view.layer.shadowColor = UIColor.black.cgColor
//        amountSheetVC.view.layer.shadowOffset = CGSize(width: 0, height: 5.0)
//        amountSheetVC.view.layer.shadowRadius = CGFloat(20.0)
//        amountSheetVC.view.layer.shadowOpacity = 0.24
        
        let view = UIView(frame: .init(x: 0, y: 0, width: 0.5, height: 0.0))
        view.backgroundColor = .green
//
        amountSheet = PullableSheet(content: amountSheetVC, topBarStyle: .custom(view))
        amountSheet?.snapPoints = [.custom(y: 340)] // snap points (if needed)
        amountSheet?.add(to: self)
        
       
    }
    
    func launchAccountSheet(){
    
    }

}



extension PaymentViewController : BottomSheetNaviagationDelegate{
    func showAccountSheet(amount: String) {
        print("Amount entered is >>>>> \(amount)")
        if !(amount.isEmpty) {
            let accountSheetVC = AccountSelectionViewController(nibName: "AccountSelection", bundle: nil)
            
            accountSheetVC.parentSheetDelegate = self
            accountSheetVC.amount = amount
            
            let view = UIView(frame: .init(x: 0, y: 0, width: 0.5, height: 0.0))
            view.backgroundColor = .green
            
            let accSheet = PullableSheet(content: accountSheetVC, topBarStyle: .custom(view))
            accSheet.snapPoints = [.custom(y: 50)] // snap points (if needed)
            accSheet.add(to: self)
            
        }
    }
    
    func showQrSheet(amount: String, pin: String) {
        print("Amount entered is >>>>> \(amount)")
        if !(amount.isEmpty) {
            let qrSheetVC = QrViewController(nibName: "QrView", bundle: nil)
            
            qrSheetVC.parentSheetDelegate = self
            qrSheetVC.amount = amount
            
            let view = UIView(frame: .init(x: 0, y: 0, width: 0.5, height: 0.0))
            view.backgroundColor = .green
            
            let qrSheet = PullableSheet(content: qrSheetVC, topBarStyle: .custom(view))
            qrSheet.snapPoints = [.custom(y: 50)] // snap points (if needed)
            qrSheet.add(to: self)
        }
    }
    
    func showSuccessSheet(amount: String) {
        print("Amount entered is >>>>> \(amount)")
        if !(amount.isEmpty) {
            let successVc = SuccessViewController(nibName: "SuccessView", bundle: nil)
            
            successVc.parentSheetDelegate = self
            successVc.amount = amount
            
            let view = UIView(frame: .init(x: 0, y: 0, width: 0.5, height: 0.0))
            view.backgroundColor = .green
            
            let qrSheet = PullableSheet(content: successVc, topBarStyle: .custom(view))
            qrSheet.snapPoints = [.custom(y: 50)] // snap points (if needed)
            qrSheet.add(to: self)
        }
    }
    
    func showExpiredSheet(amount: String) {
        print("Amount entered is >>>>> \(amount)")
        if !(amount.isEmpty) {
            let expiredVc = ExpiredViewController(nibName: "ExpiredView", bundle: nil)
            
            expiredVc.parentSheetDelegate = self
            expiredVc.amount = amount
            
            let view = UIView(frame: .init(x: 0, y: 0, width: 0.5, height: 0.0))
            view.backgroundColor = .green
            
            let qrSheet = PullableSheet(content: expiredVc, topBarStyle: .custom(view))
            qrSheet.snapPoints = [.custom(y: 50)] // snap points (if needed)
            qrSheet.add(to: self)
        }
    }
    
    func showFailureSheet(amount: String) {
        if !(amount.isEmpty) {
            let failedVc = FailureViewController(nibName: "FailureView", bundle: nil)
            
            failedVc.parentSheetDelegate = self
            failedVc.amount = amount
            
            let view = UIView(frame: .init(x: 0, y: 0, width: 0.5, height: 0.0))
            view.backgroundColor = .green
            
            let qrSheet = PullableSheet(content: failedVc, topBarStyle: .custom(view))
            qrSheet.snapPoints = [.custom(y: 50)] // snap points (if needed)
            qrSheet.add(to: self)
        }
    }
    
}



protocol BottomSheetNaviagationDelegate {
    func showAccountSheet(amount: String)
    func showQrSheet(amount:String, pin:String)
    func showSuccessSheet(amount:String)
    func showExpiredSheet(amount: String)
    func showFailureSheet(amount:String)
}
