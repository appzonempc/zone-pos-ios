//
//  SuccessViewController.swift
//  ZonePos
//
//  Created by Victor on 26/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {

    @IBOutlet var AmountLabel: UILabel!
    
    @IBOutlet var DoneBtn: UIButton!
    
    var parentSheetDelegate: BottomSheetNaviagationDelegate?
    var amount:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

      DoneBtn.layer.cornerRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let amt = amount {
            AmountLabel.text = amt
        }
    }
    


    @IBAction func doneBtnPressed(_ sender: Any) {
        if let amt = self.amount {
            parentSheetDelegate?.showExpiredSheet(amount: amt)
        }
    }
}
