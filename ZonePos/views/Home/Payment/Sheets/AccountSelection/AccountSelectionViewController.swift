//
//  AccountSelectionViewController.swift
//  ZonePos
//
//  Created by Victor on 09/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit
import KAPinField

class AccountSelectionViewController: UIViewController {

    @IBOutlet var AccountScrollView: UIScrollView!
    
    @IBOutlet var pageControl: UIPageControl!

    @IBOutlet var PinField: KAPinField!
    
    @IBOutlet var AmountLabel: UILabel!
    var slides = [Slide]()
    
    var parentSheetDelegate: BottomSheetNaviagationDelegate?
    var amount:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PinField.properties.delegate =  self
        
        AccountScrollView.delegate = self
        
        slides = createSlides()
        setupSlideScrollView(for: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(AccountScrollView)
        
        setUpPinView()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let amt = amount {
            AmountLabel.text = amt
        }
    }
    
    func setUpPinView(){
        
        PinField.properties.animateFocus = false
        PinField.properties.numberOfCharacters = 4
        PinField.properties.validCharacters = "0123456789"
        PinField.properties.isSecure = true
        PinField.appearance.kerning = 5
        PinField.appearance.textColor = UIColor.orange
        PinField.appearance.tokenColor = UIColor.black.withAlphaComponent(0.3)
        PinField.appearance.tokenFocusColor = UIColor.black.withAlphaComponent(0.3)
        
        PinField.appearance.backBorderColor = UIColor.white.withAlphaComponent(0.2)

        PinField.appearance.backRounded = false
    }
    
    func setupSlideScrollView(for : [Slide]) {
        AccountScrollView.frame = CGRect(x: 0, y: 0, width: AccountScrollView.frame.width, height: AccountScrollView.frame.height)
        AccountScrollView.contentSize = CGSize(width: AccountScrollView.frame.width * CGFloat(slides.count), height: AccountScrollView.frame.height)
        AccountScrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: AccountScrollView.frame.width * CGFloat(i), y: 0, width: AccountScrollView.frame.width, height: AccountScrollView.frame.height)
            AccountScrollView.addSubview(slides[i])
        }
    }
    
    func createSlides() -> Array<Slide>{
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.AccountType.text = "Savings"
        slide1.AccountNameLabel.text = "Samuel David"
        slide1.AccountNumberLabel.text = "2093442434"
        
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.AccountType.text = "Savings"
        slide2.AccountNameLabel.text = "Samuel King"
        slide2.AccountNumberLabel.text = "2093882434"
        
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.AccountType.text = "Current"
        slide3.AccountNameLabel.text = "Samuel Wilson"
        slide3.AccountNumberLabel.text = "2093112434"
        
        let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide4.AccountType.text = "Current"
        slide4.AccountNameLabel.text = "Samuel nelson"
        slide4.AccountNumberLabel.text = "2093002434"
        
        return [slide1, slide2, slide3, slide4]
    }
    
    
    @IBAction func PinButtonClicked(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            PinField.text?.append("1")
            
            PinField.reloadAppearance()
        case 2:
            PinField.text?.append("2")
             PinField.reloadAppearance()
        case 3:
            PinField.text?.append("3")
            PinField.reloadAppearance()
        case 4:
            PinField.text?.append("4")
            PinField.reloadAppearance()
        case 5:
            PinField.text?.append("5")
            PinField.reloadAppearance()
        case 6:
            PinField.text?.append("6")
            PinField.reloadAppearance()
        case 7:
            PinField.text?.append("7")
            PinField.reloadAppearance()
        case 8:
            PinField.text?.append("8")
           PinField.reloadAppearance()
        case 9:
            PinField.text?.append("9")
           PinField.reloadAppearance()
        case 11:
//            PinField.text?.append("0")
//          PinField.reloadAppearance()
            parentSheetDelegate?.showQrSheet(amount: AmountLabel.text ?? " ", pin: "1234")
            self.dismiss(animated: true, completion: nil)
        case 12:
            if let cnt = PinField.text?.count{
                if cnt > 0{
                   PinField.text?.removeLast()
                }
            }
            
           PinField.reloadAppearance()
        default:
            break
        }
    }
    
 
}




extension AccountSelectionViewController : UIScrollViewDelegate{
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
         setupSlideScrollView(for: slides)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
    }
}




extension AccountSelectionViewController : KAPinFieldDelegate{
    
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        
        print("code is \(code)")
    }
    
    
}







