//
//  AmountEntryViewController.swift
//  ZonePos
//
//  Created by Victor on 08/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class AmountEntryViewController: UIViewController {

    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var containingView: UIView!
    
    @IBOutlet var deleteBtn: UIButton!
    
    @IBOutlet var forwardBtn: UIButton!
    
    var parentSheetDelegate: BottomSheetNaviagationDelegate?
    
    
//    let fullView: CGFloat = 100
//    var partialView: CGFloat {
//        return UIScreen.main.bounds.height - (UIApplication.shared.statusBarFrame.height + 30)
//    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
//        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(AmountEntryViewController.panGesture))
//        view.addGestureRecognizer(gesture)

//          roundViews()
//        resizeElements()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//         prepareBackgroundView()
    }
    
  
    func resizeElements(){
        deleteBtn.imageEdgeInsets = UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50)
        forwardBtn.imageView?.contentMode = .scaleAspectFit
    }
    
    

    
    func closeSheet(){
//        UIView.animate(withDuration: 0.3, animations: {
//            let frame = self.view.frame
//            self.view.frame = CGRect(x: 0, y: self.partialView, width: frame.width, height: frame.height)
//        })
    }
    
//    func prepareBackgroundView(){
//        let blurEffect = UIBlurEffect.init(style: .dark)
//        let visualEffect = UIVisualEffectView.init(effect: blurEffect)
//        let bluredView = UIVisualEffectView.init(effect: blurEffect)
//        bluredView.contentView.addSubview(visualEffect)
//
//        visualEffect.frame = UIScreen.main.bounds
//        bluredView.frame = UIScreen.main.bounds
//
//        view.insertSubview(bluredView, at: 0)
//    }
    
//    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
//
//        let translation = recognizer.translation(in: self.view)
//        let velocity = recognizer.velocity(in: self.view)
//        let y = self.view.frame.minY
//        if ( y + translation.y >= fullView) && (y + translation.y <= partialView ) {
//            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
//            recognizer.setTranslation(CGPoint.zero, in: self.view)
//        }
//
//        if recognizer.state == .ended {
//            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
//
//            duration = duration > 1.3 ? 1 : duration
//
//            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
//                if  velocity.y >= 0 {
//                    self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
//                } else {
//                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
//                }
//
//            }, completion: nil)
//        }
//    }

    
    @IBAction func keyPadPressed(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            amountTextField.text?.append("1")
        case 2:
            amountTextField.text?.append("2")
        case 3:
            amountTextField.text?.append("3")
        case 4:
            amountTextField.text?.append("4")
        case 5:
            amountTextField.text?.append("5")
        case 6:
            amountTextField.text?.append("6")
        case 7:
            amountTextField.text?.append("7")
        case 8:
            amountTextField.text?.append("8")
        case 9:
            amountTextField.text?.append("9")
        case 10:
            if let cnt = amountTextField.text?.count{
                if cnt > 0{
                    amountTextField.text?.removeLast()
                }
            }
        case 11:
        amountTextField.text?.append("0")
        case 12:
            if let newAmount = amountTextField.text{
                parentSheetDelegate?.showAccountSheet(amount: newAmount)
                self.dismiss(animated: true, completion: nil)
            }
            
        default:
            break
        }
        
    }
    
    
}
