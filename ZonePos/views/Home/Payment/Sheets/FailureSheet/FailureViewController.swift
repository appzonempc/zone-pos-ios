//
//  FailureViewController.swift
//  ZonePos
//
//  Created by Victor on 26/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class FailureViewController: UIViewController {

    @IBOutlet var DoneBtn: UIButton!
    @IBOutlet var AmountLabel: UILabel!
    var parentSheetDelegate: BottomSheetNaviagationDelegate?
    var amount:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DoneBtn.layer.cornerRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let amt = amount {
            AmountLabel.text = amt
        }
    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
    }
    
}
