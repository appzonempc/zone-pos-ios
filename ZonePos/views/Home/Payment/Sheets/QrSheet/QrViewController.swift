//
//  QrViewController.swift
//  ZonePos
//
//  Created by Victor on 16/08/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class QrViewController: UIViewController {


    @IBOutlet var QrImage: UIImageView!
    
    @IBOutlet var PaymentStatusButton: UIButton!
    
    @IBOutlet var AmountLabel: UILabel!
    
    var parentSheetDelegate: BottomSheetNaviagationDelegate?
    var amount:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        generateQr()
        PaymentStatusButton.layer.cornerRadius = 10
//        elevateContainerview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let amt = amount {
            AmountLabel.text = amt
        }
    }
    
    func generateQr(){
        // Get define string to encode
        let myString = "https://pennlabs.org"
        // Get data from the string
        let data = myString.data(using: String.Encoding.ascii)
        // Get a QR CIFilter
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        // Input the data
        qrFilter.setValue(data, forKey: "inputMessage")
        // Get the output image
        guard let qrImage = qrFilter.outputImage else { return }
        // Scale the image
        let transform = CGAffineTransform(scaleX: 20, y: 20)
        let scaledQrImage = qrImage.transformed(by: transform)
        // Do some processing to get the UIImage
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
        
        QrImage.image = UIImage(cgImage: cgImage)
    }
    
//    func elevateContainerview(){
//
//            ContainerView.layer.masksToBounds = false
//            ContainerView.layer.shadowColor = UIColor.black.cgColor
//            ContainerView.layer.shadowOffset = CGSize(width: 0, height: 5.0)
//            ContainerView.layer.shadowRadius = CGFloat(5.0)
//            ContainerView.layer.shadowOpacity = 0.24
//    }
    
    
    
    @IBAction func PaymentStatusPressed(_ sender: UIButton) {
        
        if let amt = self.amount {
               parentSheetDelegate?.showSuccessSheet(amount: amt)
        }
        
        
    }
    

}
